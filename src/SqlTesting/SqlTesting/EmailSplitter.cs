﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

namespace SqlTesting
{
    public class EmailSplitter
    {
        [SqlFunction(IsDeterministic = true, DataAccess = DataAccessKind.None)]
        public static SqlString GetDomain(string email)
        {

            // The commented code causes two of the tests to fail in my little test harness
            // The DLL included is working completely, if you want to play with failure or expand test cases
            // just make the changes you and recompile the DLL, then use the setup to reload the assembly.

            //if (string.IsNullOrEmpty(email) || email.IndexOf('@') == -1)
            //{
            //    return null;
            //}
            //else
            //{
                var fragments = email.Split('@');
                return fragments[1].ToString();
            //}
        }
    }
}