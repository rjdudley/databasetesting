Install tSQLt
===============================
You should use a new, blank SQL Server database.

This entire demo requites tSQLt, but not SqlTest.  SqlTest will automatically add tSQLt to your database if you don't have it installed, or you can start with just tSQLt and add SqlTest later.  I really like SqlTest, it makes using tSQLt much easier and much more visuall appealing.  Having said that, major kudos to the tSQLt team for providing a great testing framework for free--thanks y'all!

If you want to start with or just use tSQLt, you can download it from http://tsqlt.org/.  

If you're interested, SqlTest has a 28-day trial period; download it from http://www.red-gate.com/products/sql-development/sql-test/.

Install Demos
=============
Create a new test class, "demo" using tSQLt.NewTestClass (or the + in SqlTest).  Run the "00 - setup" script to install some precursor functions and sprocs we'll be testing.

The rest of the tests ("01_should..." through "11_should...") can be installed one at a time.  Read them as you install them, the arrange-act-assert pattern is one you'll want to replicate, and you'll see how to use the asserts and fakes.

Three of the scripts -- the two "confirmation" and one "harness" -- do not need to be installed.  But, make sure you install the CLR assembly for the harness (installation in the comments of the harness).

Run Demos
=========
As you go through the demos and slides, be sure to read the documentation on the tSQLt site.  The docs are great.  I've added to the slodes which script you need to execute at that point.