-- this is our highly thought out migration script

INSERT INTO dbo.PersonType
        ( PersonTypeCode ,
          PersonTypeDescription
        )
VALUES  ( 'n00b' , -- PersonTypeCode - char(4)
          'Beginner'  -- PersonTypeDescription - varchar(50)
        )

GO

SELECT * FROM dbo.PersonType
WHERE PersonTypeCode = 'n00b'

GO

/*
truncate table dbo.PersonType
*/
