-- Another common scenario is adding a new column to a database
-- This test will fail the first time you run it, which means the change was not applied
-- uncomment the simulated migration script in Setup and re-run the test
-- this is the kind of test you'll want to keep exanding as you make changes to your database

CREATE PROCEDURE [demo].[test 07_should_have_same_metadata]
AS
    BEGIN

-- Setup (you probably wouldn't do this in real life,
-- you'd probably use a real migration script to create this table)

        CREATE TABLE [dbo].[DogType]
            (
              [DogTypeId] [INT] IDENTITY(1, 1)
                                NOT NULL ,
              [DogTypeCode] [CHAR](4) NOT NULL ,
              [DogTypeDescription] [VARCHAR](50) NOT NULL
            )
        ON  [PRIMARY];

        -- simulate dmigration script
        --ALTER TABLE dbo.DogType
        --ADD DogTypeLongDescription NVARCHAR(100) NULL

-- Arrange

        CREATE TABLE #expected
            (
              [DogTypeId] [INT] IDENTITY(1, 1)
                                NOT NULL ,
              [DogTypeCode] [CHAR](4) NOT NULL ,
              [DogTypeDescription] [VARCHAR](50) NOT NULL ,
              [DogTypeLongDescription] NVARCHAR(100) NULL
            );

-- Assert

        EXEC tSQLt.AssertResultSetsHaveSameMetaData 'select * from #expected',
            'select * from DogType';

-- Cleanup

        DROP TABLE dbo.DogType
        DROP TABLE #expected

    END
GO


