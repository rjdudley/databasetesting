
CREATE PROCEDURE [demo].[test 10_should_be_equal_persontype_with_fake_table]
AS
BEGIN

-- Arrange 

EXEC tSQLt.FakeTable @TableName = N'dbo.PersonType'

DECLARE @actual NVARCHAR(100)

-- Act

EXEC dbo.usp_InsertPersonType

SELECT @actual = PersonTypeDescription
FROM dbo.PersonType
WHERE PersonTypeCode = 'n00b'

-- Assert

EXEC tSQLt.AssertEquals @Expected = 'Beginner',
    @Actual = @actual


END



GO


