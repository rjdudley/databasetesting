-- This is a demonstration of how to use SpyProcedure, but not how you's realy use it
-- You would use SpyProcedure to isolate one step in a chain


CREATE PROCEDURE [demo].[test 08_should_have_spy_fake]
AS
    BEGIN

-- Use SpyProcedure to for sprocs which are called by others
-- Since they return a known value, we can set up standard test cases

-- Arrange
        DECLARE @actual VARCHAR(30)

        EXEC tsqlt.SpyProcedure @ProcedureName = N'dbo.usp_GetAnswer',
            @CommandToExecute = N'set @answer =  1;';

-- Act

        EXEC dbo.usp_GoGetAnswer '=', @actual OUTPUT

-- Asert

        EXEC tSQLt.AssertEquals @Expected = 'Hey, it worked!', -- sql_variant
            @Actual = @actual

    END
