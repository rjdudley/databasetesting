-- another advanced migration script

CREATE TABLE [dbo].[DogType](
	[DogTypeId] [INT] IDENTITY(1,1) NOT NULL,
	[DogTypeCode] [CHAR](4) NOT NULL,
	[DogTypeDescription] [VARCHAR](50) NOT NULL
) ON [PRIMARY]

GO

SELECT * FROM dbo.DogType

GO

INSERT INTO dbo.DogType
        ( DogTypeCode, DogTypeDescription )
VALUES  ( 'gldn', -- DogTypeCode - char(4)
          'Golden Retriever'  -- DogTypeDescription - varchar(50)
          )

GO

SELECT * FROM dbo.DogType
WHERE DogTypeCode = 'gldn'

go

ALTER TABLE dbo.DogType
ADD DogTypeLongDescription NVARCHAR(100)

GO

SELECT DogTypeLongDescription FROM dbo.DogType

/*
DROP TABLE dbo.DogType
*/