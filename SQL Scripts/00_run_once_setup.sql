-- These install DB objects used in tSQLt tests

CREATE FUNCTION [dbo].[AddWombat]
( @phrase NVARCHAR(50)
)
RETURNS NVARCHAR(60)
AS
BEGIN
RETURN @phrase + ' wombat'
END
GO

CREATE FUNCTION [dbo].[AddWombatFake]
( @phrase NVARCHAR(50)
)
RETURNS NVARCHAR(60)
AS
BEGIN
RETURN 'Fake wombat!'
END

GO


CREATE TABLE [dbo].[PersonType](
	[PersonTypeId] [int] NULL IDENTITY (1,1),
	[PersonTypeCode] [char](4) NULL,
	[PersonTypeDescription] [varchar](50) NULL
) ON [PRIMARY]

GO

CREATE PROCEDURE [dbo].[usp_GetAnswer]
    @direction [sys].[CHAR](1) = NULL ,
    @answer [sys].[INT] = NULL OUT
AS
    BEGIN
        SELECT  @answer = CASE @direction
                            WHEN '=' THEN 42
                            WHEN '<' THEN 41
                            WHEN '>' THEN 43
                            ELSE 0
                          END
    END
GO

CREATE PROCEDURE usp_GoGetAnswer (@direction CHAR(1), @result VARCHAR(30) OUTPUT)
AS
BEGIN

	-- This is kind of silliness, but you get the idea

    DECLARE @answer INT

    EXEC dbo.usp_GetAnswer @direction,
        @answer OUTPUT
    
    IF @answer = 1
        select @result = 'Hey, it worked!'
    ELSE
        SELECT @result = 'Bummer!'

END

GO

CREATE PROCEDURE [dbo].[usp_Frozenify](@name VARCHAR(20)) AS

-- For those of you not familar with branched logic, this is just a simple if...elsa statement
IF @name = 'Elsa'
       SELECT 'Let it go!' AS Playlist
ELSE
       SELECT 'Do you want to build a snowman?' AS Playlist

-- if you didn't think that was funny, you need to learn to let it go
GO