CREATE PROCEDURE [demo].[test 09_should_have_fake_wombat]
AS
BEGIN
-- Arrange

DECLARE @actual NVARCHAR(100)

EXEC tSQLt.FakeFunction @FunctionName = N'dbo.AddWombat', 
    @FakeFunctionName = N'dbo.AddWombatFake' 

-- Act 

EXEC @actual = dbo.AddWombat 'Howdy, '

-- Assert

EXEC tSQLt.AssertEquals 'Fake wombat!', @actual

-- Cleanup
-- tSQLt should automatically reset the functions

END


GO
