-- shows how to compare the schema and data in two tables
-- this is a good way to test new values in lookup tables
-- this can be used instead of the simple confirmation scripts
-- the advantage is being part of an automated test suite

CREATE PROCEDURE [demo].[test 05_should_be_equal_persontype]
AS
    BEGIN

-- Setup, for a clean demo only!!

        TRUNCATE TABLE dbo.PersonType;

-- Arrange 
-- This temp table is your expected value, and should contain all the lookup values

        CREATE TABLE #PersonType
            (
              PersonTypeId INT IDENTITY(1, 1) ,
              PersonTypeCode CHAR(4) ,
              PersonTypeDescription VARCHAR(50)
            )

-- Act
        INSERT  INTO #PersonType
                ( PersonTypeCode ,
                  PersonTypeDescription
                )
        VALUES  ( 'n00b' , -- PersonTypeCode - char(4)
                  'Beginner'  -- PersonTypeDescription - varchar(50)
                );

-- this is a highly thought out migration script
-- this is what we're inserting into our lookup table

        INSERT  INTO dbo.PersonType
                ( PersonTypeCode ,
                  PersonTypeDescription
                )
        VALUES  ( 'n00b' , -- PersonTypeCode - char(4)
                  'Beginner'  -- PersonTypeDescription - varchar(50)
                );

-- Assert

        EXEC tSQLt.AssertEqualsTable '#PersonType', 'PersonType'

-- Cleanup

DROP TABLE #PersonType

    END


GO


