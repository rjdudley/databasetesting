--This test is a very simple example showing how to use a basic Assert.

CREATE PROCEDURE [demo].[test 01_should_have_equality]
AS
    BEGIN
-- Arrange
        DECLARE @expected INT ,
            @actual INT;

-- Act
        SELECT  @expected = 1 ,
                @actual = 1;

-- Assert

        EXEC tSQLt.AssertEquals @expected, @actual;

    END;


GO

