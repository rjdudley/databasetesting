-- Demonstrates how to test a scalar value function.
-- This is useful, every function you have should have unit tests

CREATE PROCEDURE [demo].[test 04_should_add_wombat]
AS
BEGIN

-- Arrange
DECLARE @expected VARCHAR(50),
    @actual VARCHAR(50)

-- Act
SELECT @expected = 'howdy, wombat',
    @actual = dbo.AddWombat('howdy,')

-- Assert
EXEC tSQLt.AssertEquals @expected, @actual

END

GO


