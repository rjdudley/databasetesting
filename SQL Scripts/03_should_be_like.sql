-- Demonstrates AssertLike

CREATE PROCEDURE [demo].[test 03_should_be_like]
AS
BEGIN

-- Arrange
DECLARE @expected VARCHAR(10),
    @actual VARCHAR(10)

-- Act
SELECT @expected = '%dotnet',
    @actual = 'pghdotnet'

-- Assert
EXEC tSQLt.AssertLike @expected, @actual

END

GO




