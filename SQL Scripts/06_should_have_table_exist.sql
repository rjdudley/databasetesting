-- A common scenario is adding new tables to a database
-- this shows one way of confirming a table has been added correctly

CREATE PROCEDURE [demo].[test 06_should_have_table_exist]
AS
BEGIN
-- Setup (you probably wouldn't do this in real life,
-- you'd probably use a real migration script to create this table)

CREATE TABLE [dbo].[DogType]
    (
      [DogTypeId] [INT] IDENTITY(1, 1)
                        NOT NULL ,
      [DogTypeCode] [CHAR](4) NOT NULL ,
      [DogTypeDescription] [VARCHAR](50) NOT NULL
    )
ON  [PRIMARY];

-- Arrange

CREATE TABLE #expected
    (
      [DogTypeId] [INT] IDENTITY(1, 1)
                        NOT NULL ,
      [DogTypeCode] [CHAR](4) NOT NULL ,
      [DogTypeDescription] [VARCHAR](50) NOT NULL
    );

-- Assert

EXEC tSQLt.AssertResultSetsHaveSameMetaData 'select * from #expected',
    'select * from DogType';

-- Cleanup (again, you probably wouldn't do this in real life)

DROP TABLE dbo.DogType;
DROP TABLE #expected;

END
GO