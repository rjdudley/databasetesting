-- This demonstrates another simple Assertion
-- This will fail the first time; change the Assert and re-run

CREATE PROCEDURE [demo].[test 02_should_have_not_equality]
AS
    BEGIN
-- Arrange
        DECLARE @expected INT ,
            @actual INT;

-- Act
        SELECT  @expected = 1 ,
                @actual = 2;

-- Assert

        EXEC tSQLt.AssertEquals @expected, @actual;
        --EXEC tSQLt.AssertNotEquals @expected, @actual;

    END;



GO
