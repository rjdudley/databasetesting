
CREATE PROCEDURE [demo].[test 11_should_let_it_go]
AS
BEGIN
 
-- This is how to test a stored procedure which only returns values
-- instead of updating data. You need to trap the results in a table variable
-- and assert from the table variable.  
-- You could also set up a second table with expected results and use AssertEqualsTable

-- Arrange

DECLARE @songs TABLE(Song VARCHAR(100))
DECLARE @expected VARCHAR(100), @actual VARCHAR(100)

-- Act 
SET @expected = 'Let it go!'

INSERT INTO @songs
    EXEC dbo.usp_Frozenify @name = 'Elsa' -- varchar(20)

SELECT @actual = Song FROM @songs

-- Assert
EXEC tSQLt.AssertEquals @Expected = @expected, -- sql_variant
    @Actual = @actual, -- sql_variant
    @Message = N'Doh!' -- nvarchar(max)

END


GO


