/*

**** This part is one-time setup, the included DLL is complete and all the tests should pass.
**** If you want to play with failuew, you'll need some flavor of VS2013 to rebuild the DLL.
**** Fix the path below if necessary!

DROP FUNCTION [dbo].[GetDomain]
GO
DROP ASSEMBLY [SqlTesting]
GO

CREATE ASSEMBLY SqlTesting from 'sqltesting.dll' WITH PERMISSION_SET = SAFE;
go  
CREATE FUNCTION dbo.GetDomain(@email NVARCHAR(max))
RETURNS NVARCHAR(max) WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SqlTesting].[SqlTesting.EmailSplitter].[GetDomain]
go
*/

DECLARE @testcases TABLE
    (
      TestNumber INT IDENTITY(1, 1)
                     PRIMARY KEY ,
      TestName NVARCHAR(100) ,
      TestCase NVARCHAR(100) ,
      Expected NVARCHAR(100) ,
      Actual NVARCHAR(100),
      Result NVARCHAR(20)
    );

DECLARE @testname VARCHAR(100);
DECLARE @testcase NVARCHAR(MAX);
DECLARE @expected NVARCHAR(MAX);
DECLARE @result NVARCHAR(MAX);

DECLARE @testCaseCount INT;
DECLARE @counter INT;

SET @counter = 1;

INSERT  INTO @testcases
        ( TestName, TestCase, Expected )
VALUES  ( N'should_return_example.com', -- TestName - nvarchar(100)
          N'example@example.com', -- TestCase - nvarchar(100)
          N'example.com'  -- Expected - nvarchar(100)
          )
		,
        ( N'should_return_noone.com', -- TestName - nvarchar(100)
          N'noone@noone.com', -- TestCase - nvarchar(100)
          N'noone.com'  -- Expected - nvarchar(100)
          )
        ,
        ( N'should_return_null_from_invaid', -- TestName - nvarchar(100)
          N'noone!noone.com', -- TestCase - nvarchar(100)
          NULL  -- Expected - nvarchar(100)
          )
		,
        ( N'should_return_null_from_null', -- TestName - nvarchar(100)
          NULL, -- TestCase - nvarchar(100)
          NULL  -- Expected - nvarchar(100)
          )

SELECT  @testCaseCount = COUNT(*)
FROM    @testcases

WHILE @counter <= @testCaseCount
    BEGIN

        SELECT  @testname = TestName ,
                @expected = Expected ,
                @testcase = TestCase
        FROM    @testcases
        WHERE   TestNumber = @counter

        SELECT  @result = dbo.GetDomain(@testcase)

        IF ( @expected IS NULL )
            BEGIN
                IF ( @result IS NULL )
                    UPDATE  @testcases
                    SET     Actual = @result,
                        Result = 'True'
                    WHERE   TestNumber = @counter
                ELSE
                    UPDATE  @testcases
                    SET Actual = @result,
                        Result = 'False'
                    WHERE   TestNumber = @counter
            END
        ELSE
            BEGIN
                IF ( @expected = @result )
                    UPDATE  @testcases
                    SET Actual = @result,    
                        Result = 'True'
                    WHERE   TestNumber = @counter
                ELSE
                    UPDATE  @testcases
                    SET Actual = @result,    
                        Result = 'False'
                    WHERE   TestNumber = @counter

            END

        SET @counter = @counter + 1;

    END

SELECT  *
FROM    @testcases

 
